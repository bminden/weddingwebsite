import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from "@angular/router";
import {SessionStorageService, SessionStorage } from 'angular-web-storage';
import {Location} from '@angular/common';
import { AngularFirestore } from '@angular/fire/firestore';

declare var paypal;

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {
  item: any;
  party: any;
  email: any;
  value: any;
  title: any;
  constructor(public firestore: AngularFirestore, private router: Router, public session: SessionStorageService, private location: Location) { }

  ngOnInit(): void {
    this.item = this.session.get("item");
    this.party = this.session.get("party");
    this.email = this.session.get("email");
    this.value = this.session.get("value");
    this.title = this.session.get("title");

    this.createButton(this.value);
  }

  @ViewChild('paypal', {static: true}) paypalElement: ElementRef;

  createButton(v: String){
    paypal.Buttons({
        style: {
            shape: 'rect',
            color: 'gold',
            layout: 'vertical',
            label: 'paypal',
        },
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                amount : {
                  currency_code: 'USD',
                  value: v
                }
              }
            ]
            });
        },
        onApprove: (data, actions) => {
          return actions.order.capture().then( (details) => {
            this.writePartyInfo();
            this.router.navigateByUrl("/thanks");
          });
      }
    }).render('#paypal-button-container');
  }

  writePartyInfo(){
    this.firestore.collection("gifts").add({
      amount: this.value,
      party: this.party,
      email: this.email,
      title: this.title
    });
  }

  goBack(){
    this.location.back();
  }

}
