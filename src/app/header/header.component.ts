import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

	public links = [
		{title: 'home'},
		{title: 'registry'},
		{title: 'RSVP'},
		{title: 'travel'},
	];
	public currentUrl;

  constructor(private router: Router) {
	router.events.subscribe((event) =>  {
		if (event instanceof NavigationEnd) {
			this.currentUrl = event.url;
		}
		console.log('current url', this.currentUrl);
	});
  }

  ngOnInit(): void {
  }

}
