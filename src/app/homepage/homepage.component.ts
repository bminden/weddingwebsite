import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { find, map } from 'rxjs/operators';
import {SessionStorageService, SessionStorage } from 'angular-web-storage';

@Component({
	selector: 'app-homepage',
	templateUrl: './homepage.component.html',
	styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

	items: any = [];
	itemsLength: number = 0;

	constructor(public firestore: AngularFirestore, public session: SessionStorageService) {
		this.items = this.firestore.collection('items').valueChanges();
		let self = this;
		this.items.subscribe(function (elems) {
			console.log(elems);
			self.items = elems;
			self.itemsLength = self.items.length;
			console.log(self.itemsLength);
		});
	}

	ngOnInit() {
		
	}

	handleClick(event: Event, item: any) {
		this.session.set("item",item);
		this.session.set("title",item.Title);
	}

	getItems(): Array<any> {
		return this.items;
	}
	getItem(id: number): any {
		return this.getItems()[id];
	}

	createRange(int: number): Array<number> {
		let a = [];
		for (let i = 0; i < int; i++)
			a.push(i);
		console.log(a, int);
		return a;
	}
}
